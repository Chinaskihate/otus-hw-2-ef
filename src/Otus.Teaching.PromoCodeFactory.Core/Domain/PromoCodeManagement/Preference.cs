﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity, IEquatable<Preference>
    {
        public string Name { get; set; }

        public bool Equals(Preference other) => !(other is null) && (other.Id == Id && other.Name == Name);

        public override bool Equals(object obj) => obj is Preference other && other.Id == Id && other.Name == Name;

        public override int GetHashCode() => (Id, Name).GetHashCode();

        public List<CustomerPreference> CustomerPreferences { get; set; }
    }
}