﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity, IEquatable<Role>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Equals(Role other) => !(other is null) && (other.Id == Id && other.Description == Description && other.Name == Name);

        public override bool Equals(object obj) => obj is Role other && other.Id == Id && other.Description == Description && other.Name == Name;

        public override int GetHashCode() => (Id, Name, Description).GetHashCode();
    }
}