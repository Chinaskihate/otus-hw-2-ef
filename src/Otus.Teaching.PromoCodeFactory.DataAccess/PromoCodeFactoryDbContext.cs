﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityTypeConfigurations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PromoCodeFactoryDbContext : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Customer> Customers{ get; set; }

        public DbSet<Employee> Employees{ get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public PromoCodeFactoryDbContext()
        {

        }

        public PromoCodeFactoryDbContext(DbContextOptions<PromoCodeFactoryDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CustomerConfiguration());
            builder.ApplyConfiguration(new CustomerPreferenceConfiguration());
            builder.ApplyConfiguration(new EmployeeConfiguration());
            builder.ApplyConfiguration(new PreferenceConfiguration());
            builder.ApplyConfiguration(new PromoCodeConfiguration());
            builder.ApplyConfiguration(new RoleConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
