﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityTypeConfigurations
{
    internal class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Code).HasMaxLength(50);
            builder.Property(e => e.ServiceInfo).HasMaxLength(200);
            builder.Property(e => e.PartnerName).HasMaxLength(50);

            builder.HasOne(e => e.PartnerManager)
                .WithMany();
            builder.HasOne(e => e.Preference)
                .WithMany();
        }
    }
}
