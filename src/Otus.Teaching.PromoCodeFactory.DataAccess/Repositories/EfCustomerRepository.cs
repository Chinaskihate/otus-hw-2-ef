﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfCustomerRepository : BaseRepository<Customer>, IRepository<Customer>
    {
        public EfCustomerRepository(PromoCodeFactoryDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            var entities = await DbContext.Customers
                .Include(c => c.PromoCodes)
                .Include(c => c.CustomerPreferences)
                .ThenInclude(cp => cp.Preference)
                .ToListAsync();

            return entities;
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {

            var entity = await DbContext.Customers
                    .Include(c => c.PromoCodes)
                    .Include(c => c.CustomerPreferences)
                    .ThenInclude(cp => cp.Preference)
                    .FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }
        
        public async Task<IEnumerable<Customer>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await DbContext.Customers
                .Where(x => ids.Contains(x.Id))
                .Include(c => c.PromoCodes)
                .Include(c => c.CustomerPreferences)
                    .ThenInclude(cp => cp.Preference)
                .ToListAsync();

            return entities;
        }
    }
}
