﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfPreferenceRepository : BaseRepository<Preference>, IPreferenceRepository
    {
        public EfPreferenceRepository(PromoCodeFactoryDbContext dbContext) : base(dbContext)
        {
            
        }

        public async Task<IEnumerable<Preference>> GetAllAsync()
        {
            return await DbContext.Preferences
                .Include(p => p.CustomerPreferences)
                .ThenInclude(cp => cp.Customer)
                .ToListAsync();
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            return await DbContext.Preferences
                .Include(p => p.CustomerPreferences)
                .ThenInclude(cp => cp.Customer)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IEnumerable<Preference>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await DbContext.Preferences
                .Where(x => ids.Contains(x.Id))
                .Include(p => p.CustomerPreferences)
                .ThenInclude(cp => cp.Customer)
                .ToListAsync();
        }

        public async Task<Preference> GetByNameAsync(string name)
        {
            return await DbContext.Preferences
                .Include(p => p.CustomerPreferences)
                .ThenInclude(cp => cp.Customer)
                .FirstOrDefaultAsync(p => p.Name == name);
        }
    }
}
