﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class BaseRepository<T> where T : BaseEntity
    {
        protected readonly PromoCodeFactoryDbContext DbContext;

        public BaseRepository(PromoCodeFactoryDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<T> CreateAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            var createdResult = await DbContext.Set<T>().AddAsync(entity);
            await DbContext.SaveChangesAsync();

            return createdResult.Entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            DbContext.Set<T>().Update(entity);
            await DbContext.SaveChangesAsync();

            return entity;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var entity = await DbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
            {
                return false;
            }

            DbContext.Set<T>().Remove(entity);
            await DbContext.SaveChangesAsync();

            return true;
        }
    }
}