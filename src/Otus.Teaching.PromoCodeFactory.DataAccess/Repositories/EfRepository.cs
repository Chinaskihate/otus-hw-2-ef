﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : BaseRepository<T>, IRepository<T> where T : BaseEntity
    {
        private readonly PromoCodeFactoryDbContext _dbContext;

        public EfRepository(PromoCodeFactoryDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _dbContext.Set<T>().ToListAsync();

            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {

            var entity = await _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }
        
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _dbContext.Set<T>()
                .Where(x => ids.Contains(x.Id))
                .ToListAsync();

            return entities;
        }
    }
}