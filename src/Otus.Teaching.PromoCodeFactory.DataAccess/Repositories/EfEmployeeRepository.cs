﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfEmployeeRepository : BaseRepository<Employee>, IRepository<Employee>
    {
        public EfEmployeeRepository(PromoCodeFactoryDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            var entities = await DbContext.Employees
                .Include(e => e.Role)
                .ToListAsync();
            
            return entities;
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            var entity = await DbContext
                .Employees
                .Include(e => e.Role)
                .FirstOrDefaultAsync(x => x.Id == id)
                ;

            return entity;
        }

        public async Task<IEnumerable<Employee>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await DbContext.Employees
                .Where(x => ids.Contains(x.Id))
                .Include(e => e.Role)
                .ToListAsync();

            return entities;
        }
    }
}
