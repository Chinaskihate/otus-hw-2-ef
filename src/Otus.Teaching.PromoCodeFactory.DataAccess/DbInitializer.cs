﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DbInitializer
    {
        public static void Initialize(PromoCodeFactoryDbContext dbContext)
        {
            // dbContext.Database.EnsureDeleted();
            if (dbContext.Database.EnsureCreated())
            {
                dbContext.Employees.AddRange(FakeDataFactory.Employees);
                dbContext.Customers.AddRange(FakeDataFactory.Customers);

                // TODO: возникла проблема с тем, что в рамках одного экземпляра контекста сущность Role добавляется второй раз, выбрасывалось исключение. Почему то Entry<Role> = Detached ничего не изменял.
                var trackedRoles = FakeDataFactory.Employees
                    .Select(e => e.Role);
                var rolesToAdd = FakeDataFactory.Roles
                    .Except(trackedRoles);
                // аналогично с предпочтениями
                dbContext.Roles.AddRange(rolesToAdd);
                var trackedPreferences = FakeDataFactory.Customers
                    .SelectMany(c =>
                        c.CustomerPreferences
                            .Select(cp => cp.Preference));
                var preferencesToAdd = FakeDataFactory.Preferences
                    .Except(trackedPreferences);
                dbContext.Preferences.AddRange(preferencesToAdd);
                dbContext.SaveChanges();
            }
        }
    }
}