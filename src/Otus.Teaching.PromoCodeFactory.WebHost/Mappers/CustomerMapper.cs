﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class CustomerMapper
    {
        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences,
            Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer();
            }
            customer.CustomerPreferences.Clear();
            customer.CustomerPreferences
                .AddRange(preferences.Select(p => new CustomerPreference()
            {
                    Customer = customer,
                    CustomerId = customer.Id,
                    Preference = p,
                    PreferenceId = p.Id
            }));

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            return customer;
        }
    }
}
