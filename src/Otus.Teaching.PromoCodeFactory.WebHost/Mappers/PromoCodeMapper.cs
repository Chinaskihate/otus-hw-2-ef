﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest model)
        {
            var promoCode = new PromoCode()
            {
                Code = model.PromoCode,
                ServiceInfo = model.ServiceInfo,
                PartnerName = model.PartnerName
            };
            return promoCode;
        }
    }
}
