﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IPreferenceRepository _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository,
            IPreferenceRepository preferenceRepository, IRepository<Customer> customerRepository, IRepository<Employee> employeeRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();

            var promoCodesModelList = promoCodes.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    BeginDate = x.BeginDate.ToShortDateString(),
                    EndDate = x.EndDate.ToShortDateString(),
                    Code = x.Code,
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList();

            return promoCodesModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferenceRepository.GetByNameAsync(request.Preference);
            if (preference == null)
                return NotFound();
            if (preference.CustomerPreferences.Count == 0)
            {
                return NoContent();
            }

            var promoCode = PromoCodeMapper.MapFromModel(request);
            promoCode.Id = Guid.NewGuid();
            promoCode.Preference = preference;
            promoCode.BeginDate = DateTime.Now;
            promoCode.EndDate = DateTime.MaxValue;
            preference.CustomerPreferences
                .FirstOrDefault()
                .Customer
                .PromoCodes
                .Add(promoCode);
            await _promoCodesRepository.CreateAsync(promoCode);

            return NoContent();
        }
    }
}